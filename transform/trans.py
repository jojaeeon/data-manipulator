#!/usr/bin/env python

import sys
import argparse
import itertools
import cPickle as pickle
import operator
import re
from collections import defaultdict, OrderedDict

implicit_prefix = '__implicit__'

class CSV(object):
    def __init__(self, attr_rows, attr_cols, implicit_type = None):
        super(CSV, self).__init__()
        self.data_2d = []
        self.attr_rows = attr_rows
        self.attr_cols = attr_cols
        if implicit_type:
            self.implicit_type = implicit_type
        else:
            if not self.attr_rows:
                self.implicit_type = 'r'
            elif not self.attr_cols:
                self.implicit_type = 'c'
            else:
                self.implicit_type = None

    def load(self, path, sep):
        if self.implicit_type == 'r':
            self.data_2d.append(None)

        ncols = 0
        for (i, line) in enumerate(open(path)):
            row = []
            for (j, elem) in enumerate(line.rstrip('\n').split(sep)):
                row.append(elem)
            self.data_2d.append(row)
            ncols = max(ncols, len(row))

        if self.implicit_type == 'r':
            self.attr_rows = [0]
            self.data_2d[0] = [implicit_prefix+str(x) for x in xrange(ncols)]

    def dump(self, path, sep):
        with open(path, 'w') as fp:
            for row in self.data_2d:
                fp.write(sep.join(elem for elem in row)+'\n')

    def get_table(self):
        if self.implicit_type == 'r':
            implicit_attr_id = 0
        elif self.implicit_type == 'c':
            implicit_attr_id = len(self.attr_rows)
        else:
            implicit_attr_id = None

        table = Table(implicit_attr_id)
        ais = [0 for _ in self.attr_cols]

        for (i, row) in enumerate(self.data_2d):
            if i in self.attr_rows:
                continue
            ais = [i if self.data_2d[i][self.attr_cols[k]] != '' else ai for (k, ai) in enumerate(ais)]
            row_attrs = [self.data_2d[ai][self.attr_cols[k]] for (k, ai) in enumerate(ais)]

            ajs = [0 for _ in self.attr_rows]
            for (j, elem) in enumerate(row):
                if j in self.attr_cols:
                    continue
                ajs = [j if self.data_2d[self.attr_rows[k]][j] != '' else aj for (k, aj) in enumerate(ajs)]
                col_attrs = [self.data_2d[self.attr_rows[k]][aj] for (k, aj) in enumerate(ajs)]

                attrs = tuple(col_attrs + row_attrs)
                #print i, j, attrs, elem
                table[attrs] = elem
        return table

    def __str__(self):
        nrows = max(self.attr_rows) + 6
        ncols = max(self.attr_cols if self.attr_cols else [0]) + 6
        nrows_real = False
        ncols_real = False

        _nrows = len(self.data_2d)
        if _nrows <= nrows:
            nrows = _nrows
            nrows_real = True

        max_len = 0
        for (i, row) in itertools.izip(xrange(nrows), self.data_2d):
            _ncols = len(row)
            if _ncols <= ncols:
                ncols = _ncols
                ncols_real = True
            for (j, elem) in itertools.izip(xrange(ncols), row):
                new_len = len(str(elem))
                if new_len > max_len:
                    max_len = new_len
        max_len = str(max_len + 2)

        class CText:
            prt = ''
            def __init__(self):
                self.lvl = False
            def acq(self):
                if self.lvl == 0:
                    CText.prt += '\x1B[32m'
                self.lvl += 1
            def rel(self):
                self.lvl -= 1
                if self.lvl == 0:
                    CText.prt += '\x1B[0m'
        ctext = CText()

        for (i, row) in itertools.izip(xrange(nrows), self.data_2d):
            if self.implicit_type == 'r' and i == 0:
                continue
            if i in self.attr_rows:
                ctext.acq()
            for (j, elem) in itertools.izip(xrange(ncols), row):
                if self.implicit_type == 'c' and j == 0:
                    continue
                if j in self.attr_cols:
                    ctext.acq()
                CText.prt += ('%-'+max_len+'s') % elem
                if j in self.attr_cols:
                    ctext.rel()
            if not ncols_real:
                CText.prt += '...'
            CText.prt += '\n'
            if i in self.attr_rows:
                ctext.rel()

        if not nrows_real:
            for i in xrange(3):
                for j in xrange(ncols):
                    if j in self.attr_cols:
                        ctext.acq()
                    CText.prt += ('%-'+max_len+'s') % '.'
                    if j in self.attr_cols:
                        ctext.rel()
                if not ncols_real:
                    CText.prt += ' '*i+'.'
                CText.prt += '\n'
        CText.prt += '\x1B[0m'
        return CText.prt

class Table(object):
    def __init__(self, implicit_attr_id = None):
        super(Table, self).__init__()
        self.attr_sets = None
        self.implicit_attr_id = implicit_attr_id
        self.table = OrderedDict()

    def __setitem__(self, attrs, elem):
        if self.attr_sets is None:
            self.attr_sets = [[] for _ in attrs]
        for (attr, attr_set) in itertools.izip(attrs, self.attr_sets):
            if attr not in attr_set:
                attr_set.append(attr)
        self.table[attrs] = elem

    def __getitem__(self, attrs):
        try:
            return self.table[attrs]
        except KeyError:
            self.table[attrs] = ''
            return ''

    def load(self, path):
        with open(path) as fp:
            self.attr_sets = pickle.load(fp)
            self.implicit_attr_id = pickle.load(fp)
            self.table = pickle.load(fp)

    def dump(self, path):
        with open(path, 'w') as fp:
            pickle.dump(self.attr_sets, fp)
            pickle.dump(self.implicit_attr_id, fp)
            pickle.dump(self.table, fp)

    def get_csv(self, row_attr_idx, col_attr_idx):
        data_2d = []
        num_attr_idx = len(self.attr_sets)
        if len(row_attr_idx + col_attr_idx) != num_attr_idx:
            print >>sys.stderr, "Error: you must assign all and only indices to either rows or cols"
            attr_idx_set = set(row_attr_idx + col_attr_idx)
            prt = ''
            for attr_idx in xrange(num_attr_idx):
                if attr_idx not in attr_idx_set:
                    prt += "    %d : {" % attr_idx
                    prt += ', '.join('%r' % attrs for attrs in itertools.islice(self.attr_sets[attr_idx], 6))
                    prt += '},\n'
            sys.stderr.write(prt)
            sys.exit(1)

        def rec(prev_attr_idxs, curr_attr_idx, next_attr_idxs):
            row = []
            for _ in xrange(len(col_attr_idx)):
                row.append('')
            for _ in xrange(reduce(operator.mul, [len(self.attr_sets[k]) for k in prev_attr_idxs], 1)):
                for attr in self.attr_sets[curr_attr_idx]:
                    row.append(attr)
                    for _ in xrange(reduce(operator.mul, [len(self.attr_sets[k]) for k in next_attr_idxs], 1) - 1):
                        row.append('')
            data_2d.append(row)
            if not next_attr_idxs:
                return
            else:
                rec(prev_attr_idxs + [curr_attr_idx], next_attr_idxs[0], next_attr_idxs[1:])
        rec([], row_attr_idx[0], row_attr_idx[1:])

        prev_attr_col = (None for _ in xrange(len(col_attr_idx)))
        for attr_col in itertools.product(*[self.attr_sets[attr_col] for attr_col in col_attr_idx]):
            row = []
            for (prev_attr, attr) in itertools.izip(prev_attr_col, attr_col):
                if prev_attr != attr:
                    row.append(attr)
                else:
                    row.append('')
            prev_attr_col = attr_col

            for attr_row in itertools.product(*[self.attr_sets[attr_row] for attr_row in row_attr_idx]):
                attrs = [None for _ in xrange(num_attr_idx)]
                for (idx, attr) in itertools.izip(row_attr_idx + col_attr_idx, attr_row + attr_col):
                    attrs[idx] = attr
                try:
                    val = self.table[tuple(attrs)]
                except KeyError:
                    val = ''
                row.append(val)
            data_2d.append(row)

        csv = CSV(range(len(row_attr_idx)), range(len(col_attr_idx)), self.implicit_attr_id)
        csv.data_2d = data_2d
        return csv

    def shrink(self):
        new_table = OrderedDict()
        for (attrs, val) in self.table.iteritems():
            new_attrs = list(attrs)
            for idx in reversed(xrange(len(self.attr_sets))):
                if (len(self.attr_sets[idx]) == 1):
                    new_attrs.pop(idx)
            new_table[tuple(new_attrs)] = val
        self.table = new_table
        for idx in reversed(xrange(len(self.attr_sets))):
            if (len(self.attr_sets[idx]) == 1):
                self.attr_sets.pop(idx)

    def aggregate(self, reqs):
        if reqs == '':
            return
        for req in reqs.split(','):
            idx, func, name = req.split(':')
            idx = int(idx)
            targets = self.attr_sets[idx]
            if name != '':
                self.attr_sets[idx] = [name]
            else:
                self.attr_sets.pop(idx)

            new_table = OrderedDict()
            for (attrs, val) in self.table.iteritems():
                new_attrs = list(attrs)
                if name != '':
                    new_attrs[idx] = name
                else:
                    new_attrs.pop(idx)
                new_attrs = tuple(new_attrs)

                if new_attrs not in new_table:
                    new_table[new_attrs] = []
                new_table[new_attrs].append(val)

            for attrs in new_table.iterkeys():
                vals = new_table[attrs]

                ivals = []
                for val in vals:
                    if val == 'None':
                        ivals.append(None)
                        continue
                    try:
                        int(val)
                    except:
                        ivals.append(float(val))
                    else:
                        ivals.append(int(val))

                cnt = len(vals)
                isum = 0
                for ival in ivals:
                    if ival is None:
                        isum = None
                        break
                    isum += ival

                if isum is None:
                    new_table[attrs] = str(None)
                else:
                    if func == 'sum':
                        new_table[attrs] = str(isum)
                    elif func == 'avg':
                        new_table[attrs] = str(float(isum) / cnt)
                    elif func == 'max':
                        new_table[attrs] = str(max(ivals))
                    elif func == 'min':
                        new_table[attrs] = str(min(ivals))
            self.table = new_table

    def pick(self, picks):
        if picks == '':
            return
        for pick in picks.split(','):
            idx, fmt = pick.split(':')
            idx = int(idx)
            pat = re.compile('^'+fmt+'$')
            self.attr_sets[idx] = filter(pat.match, self.attr_sets[idx])

            new_table = OrderedDict()
            for (attrs, val) in self.table.iteritems():
                if not pat.match(attrs[idx]):
                    continue
                new_table[attrs] = val
            self.table = new_table

    def __str__(self):
        prt = 'attrs:\n'
        for (k, attr_set) in enumerate(self.attr_sets):
             prt += '    %d : {' % k
             prt += ', '.join('%r' % attrs for attrs in itertools.islice(attr_set, 6))
             if len(attr_set) > 6:
                 prt += ', ...'
             prt += '},\n'
        prt += 'elems:\n'
        for (attr, elem) in itertools.islice(self.table.iteritems(), 6):
            prt += '    %r : %r,\n' % (attr, elem)
        return prt


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        formatter_class = argparse.RawTextHelpFormatter,
        description='csv transformer',
        epilog=r'''examples:
        # Ex1) load tab-separated values from `input.txt'
        #   consider row 0,1,2 are and col 0 as attribute-rows/cols, respectively
        #   save the parsed table to `parsed.p'
        {basename} load --sep='\t' --rows=0,1,2 --cols=0 input.txt parsed.p

        # Ex2) transform parsed table in `parsed.p'
        #   consider attr 0,1 as attribute-row 0,1, and attr 3,2 as attribute-col 0,1
        #   save the comma-seperated values to `output.txt'
        {basename} trans --rows=0,1 --cols=3,2 parsed.p output.txt

    '''.format(basename = sys.argv[0]),
    )
    parser.add_argument('MODE', choices=['load', 'aggr', 'pick', 'trans'])
    if len(sys.argv) < 2:
        parser.print_help()
        sys.exit(0)
    args = parser.parse_args([sys.argv[1]])
    print args

    def trimmed_args(args):
        args.sep = args.sep.decode('string-escape')
        if args.rows is None:
            args.rows = []
        else:
            args.rows = [int(x) for x in args.rows.split(',')]
        if args.cols is None:
            args.cols = []
        else:
            args.cols = [int(x) for x in args.cols.split(',')]
        return args

    if args.MODE == 'load':
        parser = argparse.ArgumentParser(description='Load data')
        parser.add_argument('INPUT', type=str, help='path to input (csv or its cousin)')
        parser.add_argument('OUTPUT', type=str, help='path to output (python pickle)')
        parser.add_argument('--rows', metavar='row[,row...]', help='row indices of attribute rows')
        parser.add_argument('--cols', metavar='col[,col...]', help='column indices of attribute cols')
        parser.add_argument('--sep', type=str, help='seperator', default=',')

        args = trimmed_args(parser.parse_args(sys.argv[2:]))
        print args

        csv = CSV(args.rows, args.cols)
        csv.load(args.INPUT, args.sep)
        print csv
        table = csv.get_table()
        table.dump(args.OUTPUT)
        print table

    elif args.MODE == 'aggr':
        parser = argparse.ArgumentParser(description='Transform data')
        parser.add_argument('INPUT', type=str, help='path to input (python pickle)')
        parser.add_argument('OUTPUT', type=str, help='path to output (csv or its cousin)')
        parser.add_argument('--aggrs', metavar='id:func:name[,id:func:name...]',
                help='id=attr_id, func=[avg|sum], name=[new_name]')

        args = parser.parse_args(sys.argv[2:])
        print args

        table = Table()
        table.load(args.INPUT)
        table.aggregate(args.aggrs)
        table.dump(args.OUTPUT)
        print table

    elif args.MODE == 'pick':
        parser = argparse.ArgumentParser(description='Transform data')
        parser.add_argument('INPUT', type=str, help='path to input (python pickle)')
        parser.add_argument('OUTPUT', type=str, help='path to output (csv or its cousin)')
        parser.add_argument('--picks', metavar='id:fmt...[,id:fmt...]',
                help='id=attr_id, func=[avg|sum], name=[new_name]')

        args = parser.parse_args(sys.argv[2:])
        print args

        table = Table()
        table.load(args.INPUT)
        table.pick(args.picks)
        table.dump(args.OUTPUT)
        print table

    elif args.MODE == 'trans':
        parser = argparse.ArgumentParser(description='Transform data')
        parser.add_argument('INPUT', type=str, help='path to input (python pickle)')
        parser.add_argument('OUTPUT', type=str, help='path to output (csv or its cousin)')
        parser.add_argument('--rows', metavar='row[,row...]', help='id of attribute rows')
        parser.add_argument('--cols', metavar='col[,col...]', help='id of attribute cols')
        parser.add_argument('--sep', type=str, help='seperator', default=',')

        args = trimmed_args(parser.parse_args(sys.argv[2:]))
        print args

        table = Table()
        table.load(args.INPUT)
        csv = table.get_csv(args.rows, args.cols)
        print csv
        csv.dump(args.OUTPUT, args.sep)

